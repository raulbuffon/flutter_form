import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_form/adder.dart';

void main() {
  test('Test adder class', () {
    Adder adder = Adder();

    expect(adder.add(), 0);

    adder.a = 1;
    adder.b = 2;

    expect(adder.add(), 3);
  });
}