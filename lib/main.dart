import 'package:flutter/material.dart';
import 'package:flutter_form/adder.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Form Validation Demo';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  Adder adder = Adder();

  int value = 0;
  void soma(){
    setState(() {
      value = adder.add();
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            key: Key('tff1'),
            onChanged: (n) => {
              setState(() {
                adder.a = int.parse(n);
              })
            },

            //validator: (value) {
            //  if (value.isEmpty) {
            //    return 'Please enter some text';
            //  }
            //  return null;
            //},
          ),
          TextFormField(
            key: Key('tff2'),
            onChanged: (s) => {
              setState(() {
                adder.b = int.parse(s);
              })
            },
            //validator: (value) {
            //  if (value.isEmpty) {
             //   return 'Please enter some text';
              //}
              //return null;
            //},
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: (

                  ) {
                // Validate returns true if the form is valid, or false
                // otherwise.
                soma();
                if (_formKey.currentState.validate()) {
                  // If the form is valid, display a Snackbar.
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text('${adder.add().toString()}')));
                }
              },
              child: Text('Submit'),
            ),
          ),
          Text('Number: $value'),
        ],
      ),
    );
  }
}